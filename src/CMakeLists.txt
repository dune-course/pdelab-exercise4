add_executable(ldom_example ldom_example.cc)
add_dune_alberta_flags(ldom_example GRIDDIM 2)

dune_symlink_to_source_files(FILES ldomain.msh
                                   ldomain.geo
                                   ldomain.al
                                   test-alberta.cfg
                                   test-ugfactory.cg
                                   test-uggmsh.cfg
                            )
