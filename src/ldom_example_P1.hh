template<class Grid>
void ldom_example_P1( Grid& grid,
		      const int maxsteps,
		      const double TOL,
		      const double fraction,
		      const int strategy,
		      const int verbose,
		      const std::string& gridName
		      )
{
  // some types
  typedef typename Grid::LeafGridView GV;
  typedef typename Grid::ctype Coord;
  typedef double Real;
  const int dim = GV::dimension;

  // some arrays to store results
  std::vector<double> l2;
  std::vector<double> h1s;
  std::vector<double> ee;
  std::vector<int> N;

  // make finite element map
  typedef Dune::PDELab::PkLocalFiniteElementMap<GV,Coord,Real,1> FEM;
  const GV& gv=grid.leafGridView();
  FEM fem(gv);

  // note: adaptivity relies on leaf grid view object being updated by the grid on adaptation
  typedef Dune::PDELab::ConformingDirichletConstraints CON;
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;

  GFS gfs(gv,fem);
  gfs.update();
  N.push_back(gfs.globalSize());

  // make a degree of freedom vector;
  typedef typename Dune::PDELab::BackendVectorSelector<GFS,Real>::Type U;
  U u(gfs,0.0);

  // adaptive refinement loop
  for (int step=0; step<maxsteps; step++)
  {
    std::cout << std::endl;
    std::cout << "Refinement step " << step << " of " << maxsteps << std::endl;

    // get current leaf view
    const GV& gv=grid.leafGridView();

    // instantiate parameter class
    typedef LDomainExampleParameters<GV,Real> Problem;
    Problem param;

    // initialize DOFS from Dirichlet extension
    // note: currently we start from scratch on every grid
    typedef DiffusionDirichletExtensionAdapter<Problem> G;
    G g(gv,param);
    Dune::PDELab::interpolate(g,gfs,u);

    // make constraints container and initialize it
    typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
    CC cc;

    DiffusionBoundaryConditionAdapter<Problem> bctype(param);
    Dune::PDELab::constraints(bctype,gfs,cc);
    Dune::PDELab::set_nonconstrained_dofs(cc,0.0,u);
    std::cout << "constrained dofs=" << cc.size() << " of " << gfs.globalSize() << std::endl;

    // make local operator
    typedef LDomainExampleLocalOperator<Problem> LOP;
    LOP lop(param);
    typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
    MBE mbe(45); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().
    typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
    GO go(gfs,cc,gfs,cc,lop,mbe);

    // make linear solver and solve problem
    typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
    LS ls(5000,true);

    // assemble and solve linear problem
    typedef Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> SLP;
    SLP slp(go,ls,u,1e-10);
    slp.apply();

    // compute errors
    typedef Dune::PDELab::DiscreteGridFunction<GFS,U> DGF;
    DGF udgf(gfs,u);

    typedef DifferenceSquaredAdapter<G,DGF> DifferenceSquared;
    DifferenceSquared differencesquared(g,udgf);
    typename DifferenceSquared::Traits::RangeType l2errorsquared(0.0);
    Dune::PDELab::integrateGridFunction(differencesquared,l2errorsquared,10);
    l2.push_back(sqrt(l2errorsquared));
    typedef Dune::PDELab::DiscreteGridFunctionGradient<GFS,U> DGFGrad;
    DGFGrad udgfgrad(gfs,u);
    typedef ExactGradient<GV,Real> Grad;
    Grad grad(gv);
    typedef DifferenceSquaredAdapter<Grad,DGFGrad> GradDifferenceSquared;
    GradDifferenceSquared graddifferencesquared(grad,udgfgrad);
    typename GradDifferenceSquared::Traits::RangeType h1semierrorsquared(0.0);
    Dune::PDELab::integrateGridFunction(graddifferencesquared,h1semierrorsquared,10);
    h1s.push_back(sqrt(h1semierrorsquared));

    // compute estimated error
    typedef Dune::PDELab::P0LocalFiniteElementMap<Coord,Real,dim> P0FEM;
    P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::simplex,dim));
    typedef Dune::PDELab::GridFunctionSpace<GV,P0FEM,Dune::PDELab::NoConstraints,VBE> P0GFS;
    P0GFS p0gfs(gv,p0fem);
    typedef LDomainExampleResidualEstimator<Problem> ESTLOP;
    ESTLOP estlop(param);
    typedef Dune::PDELab::EmptyTransformation NoTrafo;
    typedef Dune::PDELab::GridOperator<GFS,P0GFS,ESTLOP,MBE,Real,Real,Real,NoTrafo,NoTrafo> ESTGO;
    ESTGO estgo(gfs,p0gfs,estlop,mbe);
    typedef typename Dune::PDELab::BackendVectorSelector<P0GFS,Real>::Type U0;
    U0 eta(p0gfs,0.0);
    estgo.residual(u,eta);

    // summing squares coming from each element
    Real estimated_error(0.0);
    estimated_error = eta.one_norm();
    // now take square root and push back result
    estimated_error = std::sqrt(estimated_error);
    ee.push_back(estimated_error);

    // write vtk file
    typedef Dune::PDELab::DiscreteGridFunction<P0GFS,U0> DGF0;
    DGF0 udgf0(p0gfs,eta);
    typedef DifferenceAdapter<G,DGF> Difference;
    Difference difference(g,udgf);

    //Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,2);
    Dune::VTKWriter<GV> vtkwriter(gv);
    std::stringstream fullname;
    fullname << gridName << "_output" << "_step" << step;
    vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF> >(udgf,"u_h"));
    vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<G> >(g,"u"));
    vtkwriter.addCellData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<Difference> >(difference,"u-u_h"));
    vtkwriter.addCellData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DGF0> >(udgf0,"estimated error"));

    vtkwriter.write(fullname.str(),Dune::VTK::appendedraw);

    // error control
    std::cout << "Estimated global error = " << estimated_error
        << " ( TOL = " << TOL << " )" << std::endl;
    if (estimated_error <= TOL)
    {
      std::cout << std::endl;
      std::cout << "Estimated global error below TOL. Job done." << std::endl;
      break;
    }


    // adapt grid
    if (step<maxsteps-1)
    {
      //gra.adapt(u);
      double alpha(fraction);      // refinement fraction
      double eta_alpha(0);         // refinement threshold
      double beta(0);              // coarsening fraction
      double eta_beta(0);          // coarsening threshold
      if( strategy == 1)
	Dune::PDELab::element_fraction( eta, alpha, beta, eta_alpha, eta_beta, verbose );
      else
	Dune::PDELab::error_fraction( eta, alpha, beta, eta_alpha, eta_beta, verbose );

      Dune::PDELab::mark_grid( grid, eta, eta_alpha, 0 ); // coarsening is switched off!
      Dune::PDELab::adapt_grid( grid, gfs, u, 2 );

      N.push_back(gfs.globalSize());
    }
    else
    {
      std::cout << std::endl;
      std::cout << "Max. number of refinement steps reached! Stop here." << std::endl;
    }
  } // end of adaptive refinement loop

  // print results
  std::cout << std::endl;
  std::cout << "Summary of results on the ldomain mesh: " << std::endl;
  std::cout << std::setw(10) << "level"
      << std::setw(10) << "N"
      << std::setw(12) << "l2"
      << std::setw(12) << "l2rate"
      << std::setw(12) << "h1semi"
      << std::setw(12) << "h1semirate"
      << std::setw(12) << "estimator"
      << std::setw(12) << "effectivity"
      << std::endl;

  for (std::size_t i=0; i<N.size(); i++)
    {
      double rate1=0.0;
      if (i>0) rate1=log(l2[i]/l2[i-1])/log(0.5);
      double rate2=0.0;
      if (i>0) rate2=log(h1s[i]/h1s[i-1])/log(0.5);
      std::cout << std::setw(10) << i
      << std::setw(10) << N[i]
      << std::setw(12) << std::setprecision(4) << std::scientific << l2[i]
      << std::setw(12) << std::setprecision(4) << std::scientific << rate1
      << std::setw(12) << std::setprecision(4) << std::scientific << h1s[i]
      << std::setw(12) << std::setprecision(4) << std::scientific << rate2
      << std::setw(12) << std::setprecision(4) << std::scientific << ee[i]
      << std::setw(12) << std::setprecision(4) << std::scientific << ee[i]/(h1s[i])
      << std::endl;
    }
}
