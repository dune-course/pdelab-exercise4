// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef LDOM_EXAMPLE_STRUCTS_HH
#define LDOM_EXAMPLE_STRUCTS_HH

/** \brief Traits class for convection diffusion parameters
 *
 *  A class supplying parameters to a convection-diffusion local
 *  operator has to define a public traits class exporting the needed
 *  types and constants.
 */
template<typename GV, typename RF>
struct DiffusionParameterTraits
{
  //! \brief the grid view
  typedef GV GridViewType;

  //! \brief Enum for domain dimension
  enum {
    //! \brief dimension of the domain
    dimDomain = GV::dimension
  };

  //! \brief Export type for domain field
  typedef typename GV::Grid::ctype DomainFieldType;

  //! \brief domain type
  typedef Dune::FieldVector<DomainFieldType,dimDomain> DomainType;

  //! \brief domain type
  typedef Dune::FieldVector<DomainFieldType,dimDomain-1> IntersectionDomainType;

  //! \brief Export type for range field
  typedef RF RangeFieldType;

  //! \brief range type
  typedef Dune::FieldVector<RF,GV::dimensionworld> RangeType;

  //! \brief permeability tensor type
  typedef Dune::FieldMatrix<RangeFieldType,dimDomain,dimDomain> PermTensorType;

  //! grid types
  typedef typename GV::Traits::template Codim<0>::Entity ElementType;
  typedef typename GV::Intersection IntersectionType;
};

/** Adapter that extracts boundary condition type function from parameter class
 *
 *  \tparam T model of DiffusionParameterInterface
 */
template<typename T>
class DiffusionBoundaryConditionAdapter
  : public Dune::PDELab::DirichletConstraintsParameters
{
  const T& t;

public:

  DiffusionBoundaryConditionAdapter (const T& t_) : t(t_) {}

  template<typename I>
  inline bool isDirichlet(const I& intersection, const Dune::FieldVector<typename I::ctype, I::dimension-1>& xlocal) const
  {
    return t.isDirichlet(intersection,xlocal);
  }

  template<typename I>
  inline bool isNeumann(const I& intersection, const Dune::FieldVector<typename I::ctype, I::dimension-1>& xlocal) const
  {
    return !isDirichlet(intersection,xlocal);
  }
};

/** Adapter that extracts Dirichlet boundary conditions from parameter class
 *
 *  \tparam T model of DiffusionParameterInterface
 */
template<typename T>
class DiffusionDirichletExtensionAdapter
  : public Dune::PDELab::GridFunctionBase<
                  Dune::PDELab::GridFunctionTraits<typename T::Traits::GridViewType,
                                                   typename T::Traits::RangeFieldType,
                                                   1,
                                                   Dune::FieldVector<typename T::Traits::RangeFieldType,1> >,
                                                   DiffusionDirichletExtensionAdapter<T> >
{
public:

  typedef Dune::PDELab::GridFunctionTraits< typename T::Traits::GridViewType,
                        typename T::Traits::RangeFieldType,
                        1,
                        Dune::FieldVector<typename T::Traits::RangeFieldType,1> > Traits;

  //! constructor
  DiffusionDirichletExtensionAdapter (const typename Traits::GridViewType& g_, const T& t_)
    : g(g_), t(t_) {}

  //! \copydoc GridFunctionBase::evaluate()
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    y = t.g(e,x);
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return g;
  }

private:

  const typename Traits::GridViewType& g;
  const T& t;
};

/*! Adapter that extracts gradient of exact solution from parameter class
 *
 *  \tparam T model of DiffusionParameterInterface
 */
template<typename T>
class DiffusionExactGradientAdapter
  : public Dune::PDELab::GridFunctionBase<
      Dune::PDELab::GridFunctionTraits<typename T::Traits::GridViewType,
                                       typename T::Traits::RangeFieldType,
                                       T::Traits::GridViewType::dimension,
                                       Dune::FieldVector<typename T::Traits::RangeFieldType,
                                                         T::Traits::GridViewType::dimension> >,
                                                         DiffusionExactGradientAdapter<T> >
{
public:

  typedef Dune::PDELab::GridFunctionTraits<typename T::Traits::GridViewType,
                                           typename T::Traits::RangeFieldType,
                                           T::Traits::GridViewType::dimension,
                                           Dune::FieldVector<typename T::Traits::RangeFieldType,
                                                             T::Traits::GridViewType::dimension> > Traits;

  //! constructor
  DiffusionExactGradientAdapter (const typename Traits::GridViewType& g_, const T& t_)
    : g(g_), t(t_) {}

  //! \copydoc GridFunctionBase::evaluate()
  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    y = t.gradient(e,x);
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return g;
  }

private:

  const typename Traits::GridViewType& g;
  const T& t;
};

#endif // LDOM_EXAMPLE_STRUCTS_HH
