\documentclass[american,a4paper]{article}

\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage{uebungen}
\usepackage{amsmath}
%\usepackage{tikz}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{float}

\title{\textbf{DUNE-PDELab Exercise 4\\Thursday, Feb~26th~2015 \\16:00-17:30}}
\dozent{Adrian Ngo/ Ole Klein} %Dan Popovi\'{c}
\institute{IWR, University of Heidelberg}
\semester{}
\abgabetitle{}
%\abgabeinfo{ in der Übung}
\uebungslabel{Exercise}
\blattlabel{DUNE Workshop 2015 Exercise}

\newcommand{\Laplace}{\Delta}

\begin{document}

\blatt{}{}

\begin{uebung}{\bf Adaptivity in PDELab}

In this exercise you will
\begin{itemize}
  \item implement a residual based error estimator for adaptive mesh refinement.
\end{itemize}

\vspace{1cm}
{\bf L-Domain Example:}
Consider the two-dimensional Laplace equation
\begin{align*}
  -\Laplace u(r,\theta) & = 0 & \text{ in } \Omega,
  \\
  u(r,\theta) & = g(r,\theta) = r^{\frac{2}{3}} \sin\left( \tfrac{2}{3}\theta
  \right) & \text{ on } \Gamma_D = \partial \Omega
\end{align*}
in polar coordinates
\begin{align*}
  (x,y) = (r \cdot \cos \theta, r \cdot \sin \theta)
  \qquad r \ge 0, ~~ 0 \le \theta \le \tfrac32 \pi,
\end{align*}
where the domain $\Omega$ is L-shaped as can be seen in the following
grid files (left: Gmsh mesh file {\tt ldomain.msh}, right: Alberta grid file {\tt ldomain.al}):
\begin{figure}[H]
  %\centering
  %\begin{minipage}[b]{\linewidth}
    \centering
    \includegraphics[width=0.43\linewidth]{ldomain-gmsh}\hfill
    \includegraphics[width=0.49\linewidth]{ldomain-alberta}
  %\end{minipage}
  %\caption{contents of the Alberta grid file ``ldom\_example\_grid.al'' }
\end{figure}
The function $u = g$ is the analytical solution for this boundary value problem (BVP).
The solution becomes irregular as it approaches the reentrant corner at $(0,0)$.
This is the location where we expect numerical difficulties due to large
discretization errors.

\newpage

Let $E_h^0 = \{e_0, \ldots, e_{N_h-1} \}$ be a conforming triangulation of $\Omega$.
Let $E_h^1$ denote the set of interior faces and $B_h^1$ be the set of boundary faces.

Define the conforming finite element space
\begin{align*}
  U_h^k = \left\{ u \in C^0(\overline{\Omega}) \, : \ u|_{\Omega_e} \in P_1
  ~ \forall e \in E_h^0 \right\} \subset H^1(\Omega).
\end{align*}
For $w_h \in U_h^k$, let $w_h = g$ on $\Gamma_D$ be the extension of $g$.
Let $\tilde{U}_h^k = \left\{ u \in U_h^k \, : \, u|_{\Gamma_D}=0 \right\}$.\\
The continuous Galerkin discretization of the problem then reads:

\begin{align*}
  \text{Find } u_h \in w_h + \tilde{U}_h^k \,\, : \quad r(u_h,v) = 0
  \qquad \forall v \in \tilde{U}_h^k
\end{align*}
with the residual
\begin{align*}
  r(u_h,v) = \int_\Omega \nabla u_h \cdot \nabla v ~ dx.
\end{align*}

The discretization error is defined by $$e_h := u - u_h.$$
It is a theoretical value that can only be estimated in the general
case where you don't have the exact solution $u$. Given a certain tolerance $TOL$
for the estimated error $\eta_h$, i.e. $$\eta_h \le TOL,$$ our goal is to minimize
the number of degrees of freedom $N_h$. A residual-based error estimator given in
{\it [Braess - Finite Elements: Theory, Fast Solvers and Applications in Solid Mechanics]}
reads:
\begin{align*}
  \eta_h = \Big\{ \sum\limits_{e \in E_h^0} \rho_e^2 \Big\}^{\frac{1}{2}}
\end{align*}
with {\it local error indicators}
\begin{align*}
  \rho_e := \bigg\{ h_e^2 \, \left\| R_e \right\|^2_{0,e} + \tfrac12 \sum\limits_{f \in \partial e}
  h_f \left\| R_f \right\|^2_{0,f} \bigg\}^{\frac{1}{2}} \qquad \forall e \in E_h^0,
\end{align*}
where
\begin{align*}
  R_e = \Delta u_h \qquad \forall e \in E_h^0
\end{align*}
are the {\it element-based residuals} and
\begin{align*}
  R_f = \bigg[\frac{\partial u_h}{\partial n_f}\bigg] = \bigg[ n_f \cdot \nabla u_h \bigg] \qquad \forall f \in E_h^1
\end{align*}
are the {\it face-based jumps}.

For an interior face $f \in E_h^1$, let $e^-_f$ denote the element to which $f$ belongs.
Then, $e^+_f$ denotes the neighbouring element on the other side of $f$.
The unit normalvector $n_f$ is pointing from the $e^-_f$ to $e^+_f$.
%$x_f^-$ is the center of $e_f^-$ and $x_f^+$ is the center of $e_f^+$.
%$[v]|_f = v(x_f^-) - v(x_f^+)$ is the jump of $v$ on the face $f$.
For $x \in f$, the jump of a function $w(x)$ is defined by
\begin{align*}
  [w](x) := \lim_{\epsilon\rightarrow 0+} w(x-\epsilon \cdot n_f) - \lim_{\epsilon\rightarrow 0+}
  w(x+\epsilon \cdot n_f),
\end{align*}
where $h_e$ and $h_f$ are the diameter of the element $e$ or the diameter of the face $f$ respectively.
Here, the diameter of a geometry is computed as the maximal distance between two corner points
(in the code, see {\tt ldom\_example\_estimator.hh}).

Recall that the $L^2$-norm of a function $u$ on a domain $\omega$ (could be $e$ or $f$) is
\begin{align*}
  \left\| u \right\|_{0,\omega} := \bigg( \int\limits_{\omega} u(x)^2 ~dx \bigg)^{\tfrac{1}{2}}
\end{align*}
and the $H^1$-seminorm is
\begin{align*}
  \left| u \right|_{1,\omega} := \bigg( \int \limits_{\omega} \big( \nabla u(x)
  \big)^2 ~dx \bigg)^{\tfrac{1}{2}}.
\end{align*}

It can be shown that
\begin{align*}
  \left| e \right|_{1,\omega} \leq c \cdot \eta_h
\end{align*}
with a constant $c = c(\Omega)$ depending on the domain.

The {\it effectivity index}
\begin{align*}
  I_{\text{eff}} = \frac{\eta_h}{|e_h|_{1,\omega}}
\end{align*}
is a measure for the quality of the error estimate. Ideally, it should be close to $1$.

%\newpage

The table gives an overview of the source files:

\begin{tabular}{l|p{11cm}}
  \hline
  {\tt ldom\_example.cc} & contains the main routine creating
  an L-shaped grid from
  \begin{enumerate}
  \item[(1)] the Alberta grid file {\tt ldomain.al}, or
  \item[(2)] the class defined in {\tt ldom\_example\_grid.hh}, or
  \item[(3)] the Gmsh mesh file {\tt ldomain.msh}
  \end{enumerate}
  and parsing one of the configuration file
  (1) {\tt test-alberta.cfg},
  (2) {\tt test-ugfactory.cfg} or
  (3) {\tt test-uggmsh.cfg}
  with a set of input parameters for the simulation.
  \\
  \hline
  {\tt ldom\_example\_P1.hh} & contains the driver routine that defines the
  $P_1$-Finite Elements for the solution space, runs the adaptive
  refinement loop and, for each refinement level, solves the PDE,
  computes the $L^2$-norm and the $H^1$-seminorm of the error $u-u_h$
  and calculates the estimated error $\eta_h$ and its effectivity index
  $I_{\text{eff}}$. Furthermore it plots the numerical
  solution $u_h$, the true solution $u=g$, the error $u-u_h$ and
  the estimated error $\eta_h$ for each refinement level.
  \\
  \hline
  {\tt ldom\_example\_parameters.hh} & contains a wrapper class for the BVP
  parameters and boundary conditions
  \\
  \hline
  {\tt ldom\_example\_structs.hh} & contains structures and helper
  classes to evaluate functions on the grid
  \\
  \hline
  {\tt ldom\_example\_functionadapters.hh} & contains helper classes to
  evaluate various error norms on the grid
  \\
  \hline
  %{\tt ldom\_example\_grid.hh} & contains a class reading our ALBERTA grid file
  {\tt ldom\_example\_grid.hh} & contains a class creating the domain using
  grid factory methods for UG
  \\
  \hline
  {\tt ldom\_example\_operator.hh} & contains the implementation of the
  local operator for the Standard Galerkin method
  \\
  \hline
  {\tt ldom\_example\_estimator.hh} & contains the implementation of the
  local error estimator for adaptive mesh refinement --
  {\it the only header file to be modified for this exercise}
  \\
  \hline
\end{tabular}

\vspace{1cm}
The error $\eta_h$ is assumed to be element-wise constant. Therefore, we use $P_0$ finite
elements for its calculation:
\begin{verbatim}
typedef Dune::PDELab::P0LocalFiniteElementMap<Coord,Real,dim> P0FEM;
P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::simplex,dim));
\end{verbatim}

Using the $P_0$ finite element grid function space {\tt P0GFS}

\begin{verbatim}
typedef Dune::PDELab::GridFunctionSpace<GV,P0FEM,Dune::PDELab::NoConstraints,VBE> P0GFS;
P0GFS p0gfs(gv,p0fem);
\end{verbatim}

and the $P_1$ finite element grid function space {\tt GFS} of our computed solution $u_h$,
we setup the global operator
\begin{verbatim}
typedef Dune::PDELab::EmptyTransformation NoTrafo;
typedef Dune::PDELab::GridOperator<GFS,P0GFS,ESTLOP,MBE,Real,Real,Real,NoTrafo,NoTrafo> ESTGO;
ESTGO estgo(gfs,p0gfs,estlop,mbe);
\end{verbatim}

It is used to iterate over the grid elements in order to sum up all the contribution of the
local error indicators into the global error estimator $\eta_h$ (={\tt eta}):
\begin{verbatim}
typedef typename Dune::PDELab::BackendVectorSelector<P0GFS,Real>::Type U0;
U0 eta(p0gfs,0.0);
estgo.residual(u,eta);
\end{verbatim}
The solution $u_h$ (={\tt u}) is of course also required for this calculation.

The local error indicators are computed inside the ``local operator''
\begin{verbatim}
typedef LDomainExampleResidualEstimator<Problem> ESTLOP;
ESTLOP estlop(param);
\end{verbatim}

Get familiar with at least {\tt ldom\_example\_P1.hh} and {\tt ldom\_example\_estimator.hh}.

You will see that $R_e$ is already implemented, whereas $R_f$ is only implemented for the
boundary faces. The contributions of the internal faces are still missing.

If you build and run this example now
\begin{verbatim}
make ldom_example
./ldom_example test-uggmsh.cfg
\end{verbatim}

you will end up with a wrong error estimate which is too small. Therefore the program
stops after the first iteration, telling you that the job is done.
Please add the missing part in the {\tt TODO:...} section to complete the implementation
of the residual-based error estimator and run the program again to see the correct results.
In the end, a VTK-output for each refinement level will be written to the files
{\tt uggmsh\_output\_step<level>.vtu} which you can open in Paraview:
\begin{figure}[H]
  \centering
  \begin{minipage}[b]{.9\linewidth}
    \centering
    \includegraphics[width=0.7\linewidth]{output-uggmsh}
  \end{minipage}
  %\caption{Adaptively refined grid for the L-domain example.}
\end{figure}

\end{uebung}

\end{document}
